export DOCKER_ORG ?= guardianproject
export DOCKER_IMAGE ?= $(DOCKER_ORG)/packages
export DOCKER_TAG ?= latest
export DOCKER_IMAGE_NAME ?= $(DOCKER_IMAGE):$(DOCKER_TAG)
export DOCKER_BUILD_FLAGS = 

export DEFAULT_HELP_TARGET := help/vendor
export README_DEPS ?= docs/targets.md

export DIST_CMD ?= cp -a
export DIST_PATH ?= /dist

SHELL := /bin/bash

-include $(shell curl -sSL -o .build-harness "https://gitlab.com/snippets/1957473/raw"; echo .build-harness)

all: init deps build install run

deps:
	@exit 0

## Create a distribution by coping $PACKAGES from $INSTALL_PATH to $DIST_PATH
dist: INSTALL_PATH=/usr/local/bin
dist:
	mkdir -p $(DIST_PATH)
	[ -z "$(PACKAGES)" ] || \
		( cd $(INSTALL_PATH) && $(DIST_CMD) $(PACKAGES) $(DIST_PATH) )

build:
	@make --no-print-directory docker:build

push:
	docker push $(DOCKER_IMAGE)

run:
	docker run -it ${DOCKER_IMAGE_NAME} sh

help/vendor:
	@$(MAKE) --no-print-directory -s -C vendor help

update/%:
	rm -f vendor/$(subst update/,,$@)/VERSION
	make -C vendor/$(subst update/,,$@) VERSION
	make readme

.PHONY: install
install: PACKAGES=$(sort $(dir $(wildcard vendor/*/)))
install:
	for vendor in $$(find vendor/ -mindepth 1 -maxdepth 1 -type d -exec basename {} \; | sort -u); do \
		make -C install $${vendor}; \
	done

